import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Video } from '../video-dashboard/video-dashboard.component';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss'],
})
export class VideoListComponent implements OnInit {
  @Input() videos: Video[] = [];
  @Input() selectedVideo: Video | undefined;
  @Output() onVideoSelected = new EventEmitter<Video>();

  constructor() {}

  selectVideo(video: Video) {
    this.onVideoSelected.emit(video);
  }

  ngOnInit(): void {}
}

