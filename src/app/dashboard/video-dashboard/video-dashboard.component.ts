import { Component, OnInit } from '@angular/core';

import { VideoLoaderService } from '../video-loader.service';
import { Observable } from 'rxjs';

export interface ViewDetails {
  age: number;
  region: string;
  date: string;
}

export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: ViewDetails[];
};

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent implements OnInit {
  videos: Observable<Video[]>;
  selectedVideo: Video | undefined;

  constructor(vls: VideoLoaderService) {
    this.videos = vls.getVideos();
  }

  setSelectedVideo(video: Video) {
    this.selectedVideo = video;
  }

  ngOnInit(): void {
  }

}
