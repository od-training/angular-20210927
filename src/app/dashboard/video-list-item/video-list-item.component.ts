import { Component, Input, OnInit } from '@angular/core';
import { Video } from '../video-dashboard/video-dashboard.component';

@Component({
  selector: 'app-video-list-item',
  templateUrl: './video-list-item.component.html',
  styleUrls: ['./video-list-item.component.scss']
})
export class VideoListItemComponent implements OnInit {

  @Input() video: Video | undefined;
  @Input() selected: boolean = false;

  constructor() {}

  ngOnInit(): void {
  }

}
