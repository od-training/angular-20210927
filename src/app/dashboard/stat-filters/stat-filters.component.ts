import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.scss']
})
export class StatFiltersComponent implements OnInit {
  search = new FormControl('');

  constructor(private router: Router) {
    this.search.valueChanges.pipe(
      debounceTime(200),
    ).subscribe(search => {
      this.router.navigate([], {
        queryParams: {
          search
        }
      })
    })
  }

  ngOnInit(): void {
  }

}
