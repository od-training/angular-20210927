import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Video } from './video-dashboard/video-dashboard.component';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VideoLoaderService {

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  getVideos(): Observable<Video[]> {
    const mySearch = this.route.queryParamMap.pipe(
      map(params => params.get('search') || ''),
    );

    return mySearch.pipe(
      switchMap(search => this.http.get<Video[]>('https://api.angularbootcamp.com/videos', { params: { q: search }}))
    );
  }
}
